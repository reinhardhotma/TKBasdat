<?php
    session_start();
    function connectDB() {

        $conn = pg_connect("dbname=isi user=postgres password=isi");
        
        if (!$conn) {
            die("Connection failed: " + pg_last_error());
        }
        return $conn;
    }

    function addJasa() {
        $conn = connectDB();
        $nama = $_POST['nama'];
        $lamaKirim = $_POST['lamaKirim'];
        $tarif = $_POST['tarif'];
        $exist = false;

        $sql = "INSERT INTO TOKOKEREN.JASA_KIRIM (nama, lama_kirim, tarif) VALUES ('$nama', '$lamaKirim', '$tarif')";
        $sql2 = "SELECT * FROM TOKOKEREN.JASA_KIRIM";

        if ($check = pg_query($conn, $sql2)) {
            while ($row = pg_fetch_row($check)) {
                if ($row[0] == $nama) {
                    $exist = true;
                }
            }
        } else {
            die("Error: $sql");
        }

        if ($exist) {
            echo '<script language="javascript">';
            echo 'alert("Jasa kirim sudah ada!")';
            echo '</script>';
        } else {
            if ($result = pg_query($conn, $sql)) {
                echo '<script language="javascript">';
                echo 'alert("Pembuatan jasa kirim berhasil dilakukan!")';
                echo '</script>';
                header("Location: index.php");
            } else {
                die("Error: $sql");
            }
        }
        pg_close($conn);
    }

    function addPromo() {
    	$conn = connectDB();
        $deskripsi = $_POST['deskripsi'];
        $awal = $_POST['periodeAwal'];
        $akhir = $_POST['periodeAkhir'];
        $kode = $_POST['kode'];
        $kategori = $_POST['kategori'];
        $subKategori = $_POST['subKategori'];

        $sql = "INSERT INTO TOKOKEREN.JASA_KIRIM (nama, lama_kirim, tarif) VALUES ('$nama', '$lamaKirim', '$tarif')";

        if ($check = pg_query($conn, $sql2)) {
            while ($row = pg_fetch_row($check)) {
                if ($row[0] == $nama) {
                    $exist = true;
                }
            }
        } else {
            die("Error: $sql");
        }

        if ($exist) {
            echo '<script language="javascript">';
            echo 'alert("Jasa kirim sudah ada!")';
            echo '</script>';
        } else {
            if ($result = pg_query($conn, $sql)) {
                echo '<script language="javascript">';
                echo 'alert("Pembuatan jasa kirim berhasil dilakukan!")';
                echo '</script>';
                header("Location: index.php");
            } else {
                die("Error: $sql");
            }
        }
        pg_close($conn);
    }

    function selectAllFromTable($table) {
    	$conn = connectDB();
    	
    	$sql = "SELECT * FROM $table";

    	if (!$result = pg_query($conn, $sql)) {
			die("Error: $sql");
		}
		pg_close($conn);
		return $result;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($_POST['command'] === 'addJasa') {
            addJasa();
        } else if ($_POST['command'] === 'addPromo') {
            addPromo();
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TOKOKEREN</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                	<?php
                		if (isset($_SESSION["name"])) {
                			echo '
                				<li>
                					<a href="logout.php">Logout</a>
                				<li>
                			';
                		} else {
                			echo '
                				<li>
			                        <a href="login.php">Login</a>
			                    </li>
			                    <li>
			                        <a href="register.php">Register</a>
			                    </li>
                			';
                		}
                	?>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead"><h1>TOKOKEREN</h1></p>
                <div class="list-group category">
	                <?php
	                	$category = selectAllFromTable("TOKOKEREN.kategori_utama");
	                	while ($row = pg_fetch_row($category)) {
	                		echo '<a href="#" class="list-group-item">' . $row[1] . '</a>';
	                	}
	                ?>
                </div>
                <div class="list-group">
                <?php
                	if (!isset($_SESSION["role"])) {
                		echo '';
                	} else if ($_SESSION["role"] === "admin") {
                		echo '
                			<button type="button" class="list-group-item" data-toggle="modal" data-target="#createKategori">
		                        Membuat Kategori & Sub Kategori
		                    </button>
		                    <button type="button" class="list-group-item" data-toggle="modal" data-target="#createJasaKirim">
		                        Membuat Jasa Kirim
		                    </button>
		                    <button type="button" class="list-group-item" data-toggle="modal" data-target="#createPromo">
		                        Membuat Promo
		                    </button>
		                    <button type = "button" class = "list-group-item" data-toggle = "modal" data-target = "#addPulsa">
		                        Tambah Produk Pulsa
		                    </button>
                		';
                	} else if ($_SESSION["role"] === "pelanggan") {
                		echo '
                			<button type = "button" class = "list-group-item" data-toggle = "modal" data-target = "#transaksi">
		                        Transaksi
		                    </button>
		                    <button type="button" class="list-group-item" data-toggle="modal" data-target="#createToko">
		                        Membuat Toko
		                    </button>
                		';
                	}
                ?>
                    <!-- <button type="button" class="btn btn-primary list-group-item" data-toggle="modal" data-target="#createKategori">
                        Membuat Kategori & Sub Kategori
                    </button>
                    <button type="button" class="btn btn-primary list-group-item" data-toggle="modal" data-target="#createJasaKirim">
                        Membuat Jasa Kirim
                    </button>
                    <button type="button" class="btn btn-primary list-group-item" data-toggle="modal" data-target="#createPromo">
                        Membuat Promo
                    </button>
                    <button type="button" class="btn btn-primary list-group-item" data-toggle="modal" data-target="#createToko">
                        Membuat Toko
                    </button>
                    <button type = "button" class = "btn btn-primary list-group-item" data-toggle = "modal" data-target = "#addPulsa">
                        Tambah Produk Pulsa
                    </button>
                    <button type = "button" class = "btn btn-primary list-group-item" data-toggle = "modal" data-target = "#beli">
                        Beli Produk
                    </button>
                    <button type = "button" class = "btn btn-primary list-group-item" data-toggle = "modal" data-target = "#transaksi">
                        Transaksi
                    </button>
                    <a href="beli.php"><button class = "btn btn-primary list-group-item">Beli Produk</button></a>

                    <a href="transaksi.php"><button class = "btn btn-primary list-group-item">Transaksi</button></a> -->
                </div>
            </div>
            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$24.99</h4>
                                <h4><a href="#">First Product</a>
                                </h4>
                                <p>See more snippets like this online store item at <a target="_blank" href="http://www.bootsnipp.com">Bootsnipp - http://bootsnipp.com</a>.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">15 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </p>
                                <a href="bayar.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$64.99</h4>
                                <h4><a href="#">Second Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">12 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                                <a href="bayar.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$74.99</h4>
                                <h4><a href="#">Third Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">31 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                                <a href="bayar.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$84.99</h4>
                                <h4><a href="#">Fourth Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">6 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                                <a href="bayar.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$94.99</h4>
                                <h4><a href="#">Fifth Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">18 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                                <a href="bayar.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <h4><a href="#">Like this template?</a>
                        </h4>
                        <p>If you like this template, then check out <a target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">this tutorial</a> on how to build a working review system for your online store!</p>
                        <a class="btn btn-primary" target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">View Tutorial</a>
                    </div>

                </div>

            </div>

            <div class="modal fade" id="createKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="addModalLabel">Membuat Kategori & SubKategori </h4>
                        </div>
                        <div class="modal-body">
                        <!-- <form action="detail.php" method="post"> -->
                            Kode Kategori:<br>
                            <input type="text" name="kodekategori"><br>
                            Nama Kategori:<br>
                            <input type="text" name="namakategori"><br>
                            <br>
                            <p1> Subkategori 1 </p1>
                            <br>
                            <br>
                             Kode Kategori:<br>
                            <input type="text" name="kodekategori"><br>
                            Nama Kategori:<br>
                            <input type="text" name="namakategori"><br>
                            <br>
                            <input type="submit" value="Tambah Subkategori">
                            <br>
                            <br>
                            <input type="submit" value="Submit">
                        <!--    </form> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class = "modal fade" id = "addPulsa" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel">
                <div class = "modal-dialog" role = "document">
                    <div class = "modal-content">
                        <div class = "modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="addModalLabel">Menambahkan Produk Pulsa</h4>
                        </div>
                        <div class = "modal-body">
							<form id = "tambahPulsa" method = "POST" action = "action.php">
								<div class = "form-container">
									<div class = "form-group">			
										<label for = "kodeProduk">Kode Produk</label>
										<input type = "text" class = "form-control" id = "kodeProduk" name = "kodeProduk" placeholder = "Kode Produk">			
									</div>
									<div class = "form-group">	
										<label for = "namaProduk">Nama Produk</label>
										<input type = "text" class = "form-control" id = "namaProduk" name = "namaProduk" placeholder = "Nama Produk">		
									</div>
									<div class = "form-group">
										<label for = "harga">Harga</label>
										<input type = "number" class = "form-control" id = "harga" name = "harga" placeholder = "Harga">
									</div>		
									<div class = "form-group">	
										<label for = "deskripsi">Deskripsi</label>
										<input type = "text" class = "form-control" id = "deskripsi" name = "deskripsi" placeholder = "Deskripsi">						
									</div>						
									<div class = "form-group">
										<label for = "nominal">Nominal</label>
										<input type = "number" class = "form-control" id = "nominal" name = "nominal" placeholder = "Nominal">
									</div>
									<input type = "hidden" name = "request" value = "addpulsa" />
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="createJasaKirim" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="addModalLabel">Membuat Jasa Kirim</h4>
                        </div>
                        <div class="modal-body">
                            <form action="index.php" method="post">
                                    <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control" id="namaJasaKirim" name="nama" placeholder="Nama jasa kirim" required>
                                        <label for="lamaKirim">Lama Kirim</label>
                                        <input type="text" class="form-control" id="lamaKirim" name="lamaKirim" placeholder="Estimasi lama kirim" required>
                                        <label for="tarif">Tarif</label>
                                        <input type="text" class="form-control" id="taifJasaKirim" name="tarif" placeholder="Tarif jasa kirim" pattern=[0-9]{1,} required>
                                    </div>
                                    <input type="hidden" id="add-command" name="command" value="addJasa">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="createPromo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="addModalLabel">Membuat Promo</h4>
                        </div>
                        <div class="modal-body">
                            <form action="index.php" method="post">
                                <div class="form-group">
                                    <label for="nama">Deskripsi</label>
                                    <input type="text" class="form-control" id="deskripsiPromo" name="deskripsi" placeholder="Deskripsi promo" required>
                                    <label for="nama">Periode awal</label>
                                    <input type="date" class="form-control" id="awalPromo" name="periodeAwal" required>
                                    <label for="nama">Periode akhir</label>
                                    <input type="date" class="form-control" id="akhirPromo" name="periodeAkhir" required>
                                    <label for="nama">Kode promo</label>
                                    <input type="text" class="form-control" id="kodePromo" name="kode" placeholder="Kode promo" required>
                                    <label for="nama">Kategori</label>
                                    <br>
                                    <select name="kategori" id="kategori" onchange="getId(this.value);" required>
                                        <option value="default">-- Pilih Kategori --</option>
                                        <?php
                                        	$category = selectAllFromTable("TOKOKEREN.kategori_utama");

											while ($row = pg_fetch_row($category)) {
												$noKategori = $row[0];
												$namaKategori = $row[1];
												echo '<option value='.$noKategori.'>'. $namaKategori.'</option>';
											}
                                        ?>
                                    </select>
                                    <br>
                                    <label for="nama">Sub Kategori</label>
                                    <br>
                                    <select name="subKategori" id="subKategori" required>
                                        <option value="default">-- Pilih SubKategori --</option>
                                    </select>
                                </div>
                                <input type="hidden" id="add-command" name="command" value="addPromo">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

             <div class="modal fade" id="createToko" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="addModalLabel">Membuat Toko</h4>
                        </div>
                        <div class="modal-body">
                        <!-- <form action="detail.php" method="post"> -->
                            <div id="tabCreateKurir" class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" class="form-control" id="namaToko" name="nama" placeholder="Nama toko">
                                <label for="deskripsi">Deskripsi</label>
                                <input type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi toko">
                                <label for="slogan">Slogan</label>
                                <input type="text" class="form-control" id="slogan" name="slogan" placeholder="Slogan toko">
                                <label for="lokasi">Lokasi</label>
                                <input type="text" class="form-control" id="lokasi" name="lokasi" placeholder="Lokasi toko">
                                <label for="nama">Jasa kirim :</label>
                                <select name="jasaKirim">
                                    <option value="default">-- Jasa Kirim --</option>
                                </select>
                                <br>
                            </div>
                            <button class="btn btn-primary" id="addJasaKirim">TAMBAH JASA KIRIM</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        <!--    </form> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="beli" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
               <div class="modal-dialog" role="document">
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 class="modal-title" id="addModalLabel">Beli Produk</h4>
                       </div>
                       <div class="modal-body">
                         <div class="row text-mid">
                           <form class="pulsa-barang" action="beli.php" method="post">
                             <input type='hidden' id='reqPulsa' name='daftarPulsa' value='pulsa'>
                             <input type='hidden' id='add-command' name='command' value='daftar-pulsa'>
                             <button type='submit' class='btn btn-primary borrow-details'>Pulsa</button>
                           </form>
                           <form class="pulsa-barang" action="beli.php" method="post">
                             <input type='hidden' id='reqBarang' name='daftarBarang' value='barang'>
                             <input type='hidden' id='add-command' name='command' value='daftar-barang'>
                             <button type='submit' class='btn btn-primary borrow-details'>Barang</button>
                           </form>
                         </div>
                       </div>
                   </div>
               </div>
           </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TOKOKEREN 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/function.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	function getId(val){
			$.ajax({
				type: "POST",
				url: "getData.php",
				data: "cid="+val,
				success: function(data){
					$("#subKategori").html(data);
				}
			});
		}
    </script>
</body>

</html>