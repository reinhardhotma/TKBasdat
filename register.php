<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TOKOKEREN</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                    <li>
                        <a href="register.php">Register</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->


<?php

$emailErr = $passwordErr = $ulangipasswordErr = $namalengkapErr = $noteleponErr = $alamatErr = "";
$email = $password = $ulangipassword = $namalengkap = $notelepon = $alamat = "";
if($_SERVER["REQUEST_METHOD"] == "POST") {
    
    if (empty($_POST["email"])) {
    $emailErr = "Email is required";
    } else {
    $email = test_input($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $emailErr = "Invalid email format"; 
        }
    }
    $password = test_input($_POST["password"]);
    $ulangiPassword = test_input($_POST["ulangiPassword"]);
    $namalengkap = test_input($_POST["namalengkap"]);
    if (empty($_POST["namalengkap"])) {
        $namalengkapErr = "Nama lengkap is required";
    } else {
         $namalengkap = test_input($_POST["namalengkap"]);
         if(!preg_match("/^[a-zA-Z ]*$/",$namalengkap)){
            $namalengkapErr ="Only letters and white space allowed";
         }
    }
   
    $notelepon = test_input($_POST["notelepon"]);
    $alamat = test_input($_POST["alamat"]);
    }

    function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <fieldset>
        <legend> FORM PENDAFTARAN PENGGUNA </legend>    
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        
        Email:<br>
        <input type="text" place holder = "Enter Email" name="email" required ><?php echo $emailErr;?><br>
        Password:<br>
        <input type="text" placeholder = "Enter Password " name="password" required ><br>
        Ulangi Password:<br>
        <input type="text" placeholder = "Repeat Password" name="ulangipassword" required ><br>
        Nama Lengkap:<br>
        <input type="text" placeholder = "Enter Your Full Name" name="namalengkap" required><?php echo $namalengkap;?><br>
        <div class="w3-bar">
            <div class="w3-dropdown-hover">
                <button class="w3-button">Jenis-Kelamin</button>
                <div class="w3-dropdown-content w3-bar-block w3-card-4">
                    <a href="#" class="w3-bar-item w3-button">Laki-Laki</a>
                    <a href="#" class="w3-bar-item w3-button">Perempuan</a>
                </div>
            </div>
        </div>
        No Telepon:<br>
        <input type="text" placeholder = "Phone Number " name="notelepon" required><br>
        Alamat:<br>
        <input type="text" placeholder ="Address " name="alamat" required><br>
        <input type="submit" value="Daftar">
        </form> 
    </fieldset>




    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TOKOKEREN 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/function.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
