<?php
session_start();

function connectDB() {
  //$servername = "localhost";
  $username = "postgres";
  $password = "isi";
  $dbname = "isi";

  // Create connection
  $conn = pg_connect($servername, $username, $password, $dbname);

  // Check connection
  if (!$conn) {
    die("Connection failed: " + pg_connect_error());
  }
  return $conn;
}

// select all in table function
function selectAllFromTable($tableName) {
  $conn = connectDB();

  $sql = "SELECT * FROM $tableName";

  if(!$result = pg_query($conn, $sql)) {
    die("Error: $sql");
  }
  mysqli_close($conn);
  return $result;
}

function getDetails($noInvoice)
{
  $conn = connectDB();

  $sql = "SELECT * FROM list_item WHERE no_invoice = '$noInvoice'";
  $result = pg_query($conn, $sql);

  if(!$result) {
			die("Error: $sql");
	}

  pg_close($conn);
  return $result;
}

  if ($_SERVER['REQUEST METHOD'] === 'POST') {
    # code..
  }

 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Transaksi-TOKOKEREN</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TOKOKEREN</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- tabel transaksi pulsa -->
    <div class="panel panel-default" width="100px">
      <div class="panel-heading">
        <h3><strong>Daftar Transaksi pulsa</strong></h3>
      </div>
      <div class="panel-body">
        <!-- tabel -->
        <table class="table table-bordered" id='tabel-pulsa'>
          <thead>
            <tr>
              <th>No Invoice</th>
              <th>Nama Produk</th>
              <th>Tanggal</th>
              <th>Status</th>
              <th>Total Bayar</th>
              <th>Nominal</th>
              <th>Nomor</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $table = selectAllFromTable('TRANSAKSI_PULSA');

              while ($row = pg_fetch_assoc($table)) {
                echo "<tr>";
                echo "<td>".$row['no_invoice'].'</td>';
                // echo "<td>".$row['nama_produk'].'</td>';
                echo "<td>".$row['tanggal'].'</td>';
                echo "<td>".$row['status'].'</td>';
                echo "<td>".$row['total_bayar'].'</td>';
                echo "<td>".$row['nominal'].'</td>';
                echo "<td>".$row['nomor'].'</td>';
                echo "</tr>";
              }
             ?>
          </tbody>
        </table>
      </div>
    </div>

    <!-- tabel transaksi barang -->
    <div class="panel panel-default" width="100px">
      <div class="panel-heading">
        <h3><strong>Daftar Transaksi Shipped</strong></h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>No Invoisce</th>
              <th>Nama Toko</th>
              <th>Tanggal</th>
              <th>Status</th>
              <th>Total Bayar</th>
              <th>Alamat Kirim</th>
              <th>Biaya Kirim</th>
              <th>No Resi</th>
              <th>Jasa Kirim</th>
              <th>Ulasan</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $table = selectAllFromTable('TRANSAKSI_SHIPPED');

              while ($row = pg_fetch_assoc($table)) {
                echo "<tr>";
                echo "<td>".$row['no_invoice'].'</td>';
                echo "<td>".$row['nama_toko'].'</td>';
                echo "<td>".$row['tanggal'].'</td>';
                echo "<td>".$row['status'].'</td>';
                echo "<td>".$row['total_bayar'].'</td>';
                echo "<td>".$row['alamat_kirim'].'</td>';
                echo "<td>".$row['biaya_kirim'].'</td>';
                echo "<td>".$row['no_resi'].'</td>';
                echo "<td>".$row['nama_jasa_kirim'].'</td>';
                // trigger button ulasan
                echo "<td>
                        <div class='row'>
                        <form action='book_details.php' method='post'>
                          <input type='hidden' id='noInvoice' name='invoice' value='".$row['no_invoice']."'>
                          <input type='hidden' id='add-command' name='command' value='show_details'>
                          <button type='submit' class='btn btn-primary transaction-details' data-toggle='modal' data-target='#modal-details'>Daftar Produk</button>
                        </form>
                        </div>
                      </td>";
                echo "</tr>";
              }
             ?>
          </tbody>
        </table>
      </div>
    </div>

    <div class="modal fade" id="modal-details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">DAFTAR PRODUK DIBELI</h4>
          </div>
          <div class="modal-body">
            <p><strong>No. Invoice:</strong><?php echo $invoice_details; ?></p>
            <table>

            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="transaksi.php"><button type="button" class="btn btn-primary">Bayar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="panel panel-default" width="100px">
      <div class="panel-heading">
        <h3>Tagihan Transaksi</h3>
      </div>
      <div class="panel-body">
        <p>No. transaksi: </p>
        <p>Tgl. Transaksi: </p>
        <p>Bank Pembayaran: </p>
        <p><strong>Status Pembayaran: </strong></p>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Detail Pembayaran</h4>
          </div>
          <div class="panel-body">
            <div>
              Harga total barang
              <p class="text-right">Rp. 0</p>
            </div>
            <div>
              Biaya pengiriman
              <p class="text-right">Rp. 0</p>
            </div>
            <div>
              <strong>Total pembayaran</strong>
              <p class="text-right"><strong>Rp. 0</strong></p>
            </div>
          </div>
        </div>
        <div class="panel panel-default" width="100px">
          <div class="panel-heading">
            <h4>Detail Barang Dibeli</h4>
          </div>
          <div class="panel-body">
            <div class="media">
              <div class="media-left media-top">
                <a href="#">
                  <img class="media-object" src="#" alt="#">
                </a>
              </div>
              <div class="media-body">
                <p><strong>Nama Toko<strong></p>
                <p><strong>Alamat Toko: </strong></p>
                <p><strong>Nama barang<strong></p>
                <p>Qty: </p>
                <p>Harga: </p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default" width="100px">
          <div class="panel-heading">
            <h4>Data Pembeli</h4>
          </div>
          <div class="panel-body">
            <p><strong>id user: </strong></p>
            <p>Nama: </p>
            <p>No. Telp: </p>
            <p>Email: </p>
            <p>Alamat Pembeli: </p>
          </div>
        </div>
      </div>
    </div> -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TOKOKEREN 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
