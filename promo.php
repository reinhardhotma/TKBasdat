<?php	
	$errDeskripsi = "";
	$errPeriodeAwal = "";
	$errPeriodeAkhir = "";
	$errKategoriUtama = "";
	$errKodePromo = "";
	$errSubKategori = "";
	$validate = true;
	session_start();
	$_SESSION['email'] = 'aabyss5i@dropbox.com';

	function connectDB() {
		// Create connection
		$conn = pg_connect("dbname=reinhardhotma user=postgres password=bajinganloah");
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + pg_last_error());
		}
		return $conn;
	}

	function submitPromo(){
		$conn = connectDB();
		
		$deskripsi = $_POST['isiDeskripsi'];
		$periodeAwal = $_POST['periodeAwal'];
		$periodeAkhir = $_POST['periodeAkhir'];
		$kodePromo = $_POST['kodePromo'];
		$kategoriUtama = $_POST['kategoriUtama'];
		$subKategori = $_POST['subKategori'];
		$idPromo;
		$sql4 = "SELECT id FROM tokokeren.PROMO";

		if(!$result4 = pg_query($conn, $sql4)) {
			die("Error: $sql4");
		}

		while ($row = pg_fetch_row($result4)) {
			$idPromo = $row[0];
		}

		$newIdPromo = generateId($idPromo);

		$sql1 = "INSERT into tokokeren.PROMO (id, deskripsi, periode_awal, periode_akhir, kode) values ('$newIdPromo', '$deskripsi', '$periodeAwal', '$periodeAkhir', '$kodePromo')";

		if(!$result1 = pg_query($conn, $sql1)) {
			die("Error: $sql1");
		}

		$sql2 = "SELECT * FROM tokokeren.SHIPPED_PRODUK WHERE kategori = '$subKategori'";

		if(!$result2 = pg_query($conn, $sql2)) {
			die("Error: $sql2");
		}

		while ($row = pg_fetch_row($result2)) {
			$kode_produk = $row[0];
			$sql3 = "INSERT INTO tokokeren.PROMO_PRODUK (id_promo, kode_produk) values ('$newIdPromo', '$kode_produk')";

			if(!$result3 = pg_query($conn, $sql3)) {
				die("Error: $sql3");
			}
		}

		// header("Location: index.php");
	}

	function generateId($idPromo){
		$idPromoDpn = substr($idPromo, 0, 1);
		$idPromoBlkng = substr($idPromo, 1);

		$newIdPromo = "1".$idPromoBlkng;
		$newIdPromo += 1;
		$newIdPromo = substr($newIdPromo, 1);
		$newIdPromo = $idPromoDpn.$newIdPromo;

		// $idPromoBlkng += 1;
		// $panjangNumberExist = strlen($idPromoBlkng);
		// $newIdPromo = $idPromoBlkng;

		// for ($i = $panjangNumberExist; $i < $panjangNumber; $i++) { 
		// 	$newIdPromo = "0".$newIdPromo;
		// 	$panjangNumberExist = strlen($newIdPromo);
		// }

		// $newIdPromo = $idPromoDpn.$newIdPromo;
		return $newIdPromo;
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if (empty($_POST["isiDeskripsi"])) {
			$errDeskripsi = "Description is required";
			$validate = false;
		}
		  
		if (empty($_POST["periodeAwal"])) {
			$errPeriodeAwal = "Awal periode is required";
			$validate = false;
		}

		if (empty($_POST["periodeAkhir"])) {
			$errPeriodeAkhir = "Akhir periode is required";
			$validate = false;
		} elseif ($_POST["periodeAkhir"] < $_POST["periodeAwal"]) {
			$errPeriodeAkhir = "Periode akhir harus lebih lama daripada periodeAwal";
			$validate = false;
		}

		if (empty($_POST["kodePromo"])) {
			$errKodePromo = "Kode promo is required";
			$validate = false;
		} elseif (strlen($_POST["kodePromo"]) > 20) {
			$errKodePromo = "Panjang karakter kode promo tidak boleh melebihi 20 karakter";
			$validate = false;
		}

		if ($_POST["kategoriUtama"] == "noChoice") {
			$errKategoriUtama = "Kategori Utama is required";
			$validate = false;
		}

		if ($_POST["subKategori"] == "noChoice") {
			$errSubKategori = "Sub Kategori is required";
			$validate = false;
		}

		if($validate == true){
			submitPromo();
		}
	}

?>

<!DOCTYPE html>
<html lang="en">
		<title>Promo</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="libs/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="src/css/base.css" >
	</head>
	<body>
		<nav class="navbar-default navbar-inverse">
	    	<div class="navbar-center"><span class="lead big-text">Toko<b>Keren</b></span></div>
	   		<div class="collapse navbar-collapse" id="navbar-collapse-1">
	      		<ul class="nav navbar-nav navbar-right">
	        		<li class="dropdown">
	          			<a href="#" class="dropdown-toggle medium-text" data-toggle="dropdown" style="color: white">Sign in as <b>
	          			<?php 
	          				if(isset($_SESSION["email"])){
	          					echo $_SESSION["email"];
	          				} else {
	          					header("Location: index.php");
	          				}
	          			?></b><b class="caret"></b></a>
	          			<ul class="dropdown-menu">
	            			<li><a href="admin-landing.php" class="medium-text">Back to main menu</a></li>
	            			<li><a href="logout.php" class="medium-text">Sign Out</a></li>
	          			</ul>
	       			</li>
	     	 	</ul>
	    	</div>
		</nav>
		<div class="content">
			<div class="container" style="padding-left: 20%; padding-right: 20%">
				<div class="konten-border">
					<div class="konten-header text-center">
						<span class="header-text"> FORM PROMO </span>
					</div>
					<div class="konten-isi" style="text-align: justify; text-justify: inter-word;">
						<form action="promo.php" method="post">
							<div class="form-group">
								<label for="deskripsi">Deskripsi<span class="required" style="color: red">*</span></label>
								<input type="text" class="form-control" id="deskripsi" name="isiDeskripsi">
								<span style="color: red"><?php echo $errDeskripsi; ?></span>
							</div>
							<div class="form-group">
								<label for="periodeAwal">Periode Awal<span class="required" style="color: red">*</span></label>
								<input type="date" class="form-control" id="periodeAwal" name="periodeAwal">
								<span style="color: red"><?php echo $errPeriodeAwal; ?></span>
							</div>
							<div class="form-group">
								<label for="periodeAkhir">Periode Akhir<span class="required" style="color: red">*</span></label>
								<input type="date" class="form-control" id="periodeAkhir" name="periodeAkhir">
								<span style="color: red"><?php echo $errPeriodeAkhir; ?></span>
							</div>
							<div class="form-group">
								<label for="kodePromo">Kode Promo<span class="required" style="color: red">*</span></label>
								<input type="text" class="form-control" id="kodePromo" name="kodePromo">
								<span style="color: red"><?php echo $errKodePromo; ?></span>
							</div>
							<div class="form-group">
								<label for="kategori">Pilih Kategori<span class="required" style="color: red">*</span></label>
								<select class="form-control" name="kategoriUtama" onchange="getId(this.value);">
									<option value="noChoice">Select Kategori Utama</option>
									<?php
										$conn = connectDB();
										$sql = "SELECT * FROM tokokeren.KATEGORI_UTAMA";

										if(!$result = pg_query($conn, $sql)) {
											die("Error: $sql");
										}

										while ($row = pg_fetch_row($result)) {
											$noKategori = $row[0];
											$namaKategori = $row[1];
											echo '<option value='.$noKategori.'>'. $namaKategori.'</option>';
										}
									?>
								</select>
								<span style="color: red"><?php echo $errKategoriUtama; ?></span>
							</div>
							<div class="form-group">
								<select class="form-control" name="subKategori" id="subKategori">
									<option value="noChoice">Select Sub-Kategori</option>
								</select>
								<span style="color: red"><?php echo $errSubKategori; ?></span>
							</div>
						  	<button type="submit" class="btn btn-default" name="command" id="command">Submit</button>
						</form>
						<div>
							<span class="required" style="color: red">*required</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="text-center footer">
			<span class="lead" style="color:white; font-size:100%">Tugas Kelompok Basis Data</span>
		</div>
		
		<script src="libs/jquery/dist/jquery.min.js"></script>
		<script src="libs/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			function getId(val){
				$.ajax({
					type: "POST",
					url: "getdata.php",
					data: "cid="+val,
					success: function(data){
						$("#subKategori").html(data);
					}
				});
			}
		</script>
	</body>
</html>