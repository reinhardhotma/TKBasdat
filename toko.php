<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TOKOKEREN</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                    <li>
                        <a href="register.php">Register</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
				<div class = "well">
					<p class="lead"><h1>Nama Toko</h1></p> <hr />
					<div id = "informasi_toko">
						<p>Pemilik Toko : (penjual)</p>
						<p>Deskripsi : (deskripsi)</p>
						<p>Slogan : (slogan)</p>
						<p>Alamat : (alamat)</p>
					</div>
				</div>
                <div id = "dropdown-kategori-subkategori" class = "list-group">

					<div class = "dropdown">
						<button class = "btn btn-primary dropdown-toggle list-group-item" type = "button" id = "kategori" data-toggle = "dropdown">
							Kategori
							<span class = "caret"></span>
						</button>
						<ul class = "dropdown-menu" role = "menu" aria-labelledby = "kategori">
							<li role = "presentation"><a href = "#">Kategori 1</a></li>
							<li role = "presentation"><a href = "#">Kategori 2</a></li>
							<li role = "presentation"><a hefr = "#">Kategori 3</a></li>
						</ul>
					</div>

					<div class = "dropdown">
						<button class = "btn btn-primary dropdown-toggle list-group-item" type = "button" id = "subkategori" data-toggle = "dropdown">
							Subkategori
							<span class = "caret"></span>
						</button>
						<ul class = "dropdown-menu" role = "menu" aria-labelledby = "subkategori">
							<li role = "presentation"><a href = "#">Subkategori 1</a></li>
							<li role = "presentation"><a href = "#">Subkategori 2</a></li>
							<li role = "presentation"><a href = "#">Subkategori 3</a></li>
						</ul>
					</div>

					<button class = "btn btn-primary list-group-item" data-toggle = "modal" data-target = "#addProduk">Tambah Shipped Produk</button>
          
				</div>
            </div>
            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$24.99</h4>
                                <h4><a href="#">First Product</a>
                                </h4>
                                <p>See more snippets like this online store item at <a target="_blank" href="http://www.bootsnipp.com">Bootsnipp - http://bootsnipp.com</a>.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">15 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </p>
                                <a href="beli.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$64.99</h4>
                                <h4><a href="#">Second Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">12 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                                <a href="beli.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$74.99</h4>
                                <h4><a href="#">Third Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">31 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                                <a href="beli.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$84.99</h4>
                                <h4><a href="#">Fourth Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">6 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                                <a href="beli.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$94.99</h4>
                                <h4><a href="#">Fifth Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">18 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                                <a href="beli.php"><button type="button" class="btn btn-primary">Beli</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <h4><a href="#">Like this template?</a>
                        </h4>
                        <p>If you like this template, then check out <a target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">this tutorial</a> on how to build a working review system for your online store!</p>
                        <a class="btn btn-primary" target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">View Tutorial</a>
                    </div>

                </div>

            </div>

			 <div class="modal fade" id="addProduk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="addModalLabel">Menambahkan Shipped Produk</h4>
						</div>
						<div class="modal-body">
						<!-- <form action="detail.php" method="post"> -->
							<form id = "tambahProduk" method = "POST" action = "action.php">
								<div class = "form-container">
									<div class = "form-group">	
										<label for="kodeProduk">Kode Produk</label>
										<input type="text" class="form-control" id="kodeProduk" name="kodeProduk" placeholder="Kode Produk">
									</div>
									<div class = "form-group">
										<label for="namaProduk">Nama Produk</label>
										<input type="text" class="form-control" id="namaProduk" name="namaProduk" placeholder="Nama Produk">
									</div>
									<div class = "form-group">
										<label for="harga">Harga</label>
										<input type="number" class="form-control" id="hargaProduk" name="harga" placeholder="Harga">
									</div>
									<div class = "form-group">
										<label for="deskripsi">Deskripsi</label>
										<input type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi">
									</div>
									<div class = "form-group">
										<label for="Subkategori">Subkategori</label>
										<select class="form-control" id="subkategori" name="subkategori" placeholder="Subkategori">
											<option value = "default">--Pilih Subkategori--</option>
										</select>
									</div>
									<div class = "form-group">
										<label for="asuransi">Barang Asuransi</label>
										<select class = "form-control" id = "asuransi" name = "asuransi" placeholder = "Barang Asuransi">
											<option value = "default">--Barang Asuransi--</option>
											<option value = "true">Ya</option>
											<option value = "false">Tidak</option>
										</select>
									</div>
									<div class = "form-group">
										<label for="stok">Stok</label>
										<input type="number" class="form-control" id="stok" name="stok" placeholder="Stok">
									</div>
									<div class = "form-group">
										<label for="baru">Barang Baru</label>
										<select class = "form-control" id = "baru" name = "baru" placeholder = "Barang Baru">
											<option value = "default">--Barang Baru--</option>
											<option value = "true">Ya</option>
											<option value = "false">Tidak</option>
										</select>
									</div>
									<div class = "form-group">
										<label for="minOrder">Minimal Order</label>
										<input type="number" class="form-control" id="minOrder" name="minOrder" placeholder="Minimal Order">
									</div>
									<div class = "form-group">
										<label for="minGrosir">Minimal Grosir</label>
										<input type="number" class="form-control" id="minGrosir" name="minGrosir" placeholder="Minimal Grosir">
									</div>
									<div class = "form-group">
										<label for="maxGrosir">Maksimal Grosir</label>
										<input type="number" class="form-control" id="maxGrosir" name="maxGrosir" placeholder="Maksimal Grosir">
									</div>
									<div class = "form-group">
										<label for="grosir">Harga Grosir</label>
										<input type="number" class="form-control" id="grosir" name="grosir" placeholder="Harga Grosir">
									</div>
									<div class = "form-group">
										<label for="lokasi">Foto</label>
										<input type="file" class="form-control" id="foto" name="foto" placeholder="Foto">
									</div>
									<input type = "hidden" name = "request" value = "addproduk" />
									<input type = "hidden" name = "namatoko" value = "<namatoko>" />
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TOKOKEREN 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/function.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
