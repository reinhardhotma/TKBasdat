<?php
    session_start();

	if (isset($_SESSION["name"])) {
		header("Location: index.php");
	}

    function connectDB() {

        $conn = pg_connect("dbname=isi user=postgres password=isi");
        
        if (!$conn) {
            die("Connection failed: " + pg_last_error());
        }
        return $conn;
    }

    function login() {
    	$email = $_POST['email'];
		$password = $_POST['password'];
		$conn = connectDB();
		$sql = "SELECT * FROM TOKOKEREN.pengguna";
		$flag = false;
		if ($result = pg_query($conn, $sql)) {
			while ($row = pg_fetch_row($result)){
				foreach ($row as $key => $value) {
					if ($email == $row[0] && $password == $row[1]) {
						$sql2 = "SELECT * FROM TOKOKEREN.pelanggan WHERE email='$email' LIMIT 1";
						$result2 = pg_query($conn, $sql2);
						$row2 = pg_fetch_row($result2);
						if (pg_fetch_row($result2) == false) {
							$_SESSION["role"] = "admin";
						} else if ($row2 = pg_fetch_row($result2)) {
							if ($row2['1']) {
								$_SESSION["role"] = "penjual";
							} else {
								$_SESSION["role"] = "pelanggan";
							}
						}
						$_SESSION["name"] = $row[2];
						
						header("Location: index.php");
						$flag = true;
					}
				}
			}
		}
		if (!$flag) {
			echo '<script language="javascript">';
			echo 'alert("Login Failed!")';
			echo '</script>';
		}
		pg_close($conn);
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($_POST['command'] === 'addJasa') {
            addJasa();
        } else if ($_POST['command'] === 'login') {
            login();
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TOKOKEREN</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                    <li>
                        <a href="register.php">Register</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <fieldset>
            <form action="login.php" method="post">
                <legend>Form Register</legend>
                Email:<br>
                <input type="text" name="email" placeholder="Email" required><br>
                Password:<br>
                <input type="password" name="password" placeholder="Password" required><br>
                <br>
                <input type="hidden" id="add-command" name="command" value="login">
                <input type="submit" value="Login">
            </form>
        </fieldset>
    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TOKOKEREN 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/function.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>