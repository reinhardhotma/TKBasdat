<?php
session_start();

function connectDB() {
  //$servername = "localhost";
  $username = "postgres";
  $password = "isi";
  $dbname = "isi";

  // Create connection
  $conn = pg_connect($servername, $username, $password, $dbname);

  // Check connection
  if (!$conn) {
    die("Connection failed: " + pg_connect_error());
  }
  return $conn;
}

// public function getPengguna($id)
// {
//   # code...
// }
 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BELI PRODUK-TOKOKEREN</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TOKOKEREN</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="panel panel-default" width="100px">
      <div class="panel-heading">
        <h3 class="panel-title text-center"><strong>Beli Produk</strong></h3>
      </div>
      <div class="panel-body">
        <div class="row text-center">
          <button id="pulsa-btn" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#data-pulsa" aria-expanded="false" aria-controls="data-pulsa">
            Pulsa
          </button>
          <button id="barang-btn" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#data-barang" aria-expanded="false" aria-controls="data-barang">
            Barang
          </button>
        </div>
      </div>
    </div>

    <div class="panel panel-default collapse" id='data-pulsa' width='100px'>
      <div class="panel-heading">
        <h3><strong>Daftar Pulsa</strong></h3>
      </div>
      <div class="panel-body">
        <div class="barang" id="data-pulsa">
            <!-- table pulsa-->
            <table id="pulsa-table" class="table table-bordered">
              <thead>
                <tr>
                  <th>Kode Produk</th>
                  <th>Nama Produk</th>
                  <th>Harga</th>
                  <th>Deskripsi</th>
                  <th>Nominal</th>
        					<th></th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>

            <!-- submit button -->

        </div>
      </div>
    </div>

    <div class="panel panel-default collapse" id='data-barang' width='100px'>
      <div class="panel-heading">
        <h3><strong>Daftar Barang</strong></h3>
      </div>
      <div class="panel-body">
        <div class="barang" id="data-pulsa">
          <div class="well">
            <!-- tabel barng -->
            <table>

            </table>

            <!-- submit button -->

          </div>
        </div>
      </div>
    </div>

    <!-- <div class="row">
      <div class="col-xs-12 col-md-8">
        <div class="panel panel-default" width="100px">
          <div class="panel-heading">
            <h3 class="panel-title"><strong>Detail Pembeli</strong></h3>
          </div>
          <div class="panel-body">
            <form>
              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" >
              </div>
              <div class="form-group">
                <label for="no-telp">Telepon/Handphone</label>
                <input type="text" class="form-control" id="no-telp">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email">
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea type="text" class="form-control" id="alamat"></textarea>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-md-4 pull-right">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><strong>Ringkasan Belanja</strong></h3>
          </div>
          <div class="panel-body">
            <div>
              Harga total barang
              <p class="text-right">Rp. 0</p>
            </div>
            <div>
              Biaya pengiriman
              <p class="text-right">Rp. 0</p>
            </div>
            <div>
              <strong>Total pembayaran</strong>
              <p class="text-right"><strong>Rp. 0</strong></p>
            </div>
            <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#modal-bayar">Bayar Lewat Bank</button>
          </div>
        </div>
      </div>
    </div>

    <div class="row pull-left">
      <div class="col-xs-12 col-md-8">
        <div class="panel panel-default" width="100px">
          <div class="panel-heading">
            <h3 class="panel-title"><strong>Detail Barang Belanja</strong></h3>
          </div>
          <div class="panel-body">
            <div class="panel panel-default" width="100px">
              <div class="panel-heading">
                <h3 class="panel-title"><strong>Nama Toko</strong></h3>
              </div>
              <div class="panel-body">
                <div class="media">
                  <div class="media-left media-middle">
                    <a href="#">
                      <img class="media-object" src="#" alt="#">
                    </a>
                  </div>
                  <div class="media-body">
                    <strong>Nama Produk</strong>
                    <br>
                    Qty
                    <p class="text-right">1</p>
                    Harga
                    <p class="text-right">Rp. 0</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <!-- <div class="modal fade" id="modal-bayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Pembayaran</h4>
          </div>
          <div class="modal-body">
            <div class="radio">
              <label><input type="radio" name="optradio">Bank 1</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="optradio">Bank 2</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="optradio">Bank 3</label>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="transaksi.php"><button type="button" class="btn btn-primary">Bayar</button>
          </div>
        </div>
      </div>
    </div> -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TOKOKEREN 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
