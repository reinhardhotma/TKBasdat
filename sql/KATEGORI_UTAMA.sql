INSERT INTO KATEGORI_UTAMA VALUES ('K01','Fashion Wanita');
INSERT INTO KATEGORI_UTAMA VALUES ('K02','Fashion Pria');
INSERT INTO KATEGORI_UTAMA VALUES ('K03','Fashion Muslim');
INSERT INTO KATEGORI_UTAMA VALUES ('K04','Fashion Anak');
INSERT INTO KATEGORI_UTAMA VALUES ('K05','Kecantikan');
INSERT INTO KATEGORI_UTAMA VALUES ('K06','Kesehatan');
INSERT INTO KATEGORI_UTAMA VALUES ('K07','Ibu & Bayi');
INSERT INTO KATEGORI_UTAMA VALUES ('K08','Rumah Tangga');
INSERT INTO KATEGORI_UTAMA VALUES ('K09','Handphone & Tablet');
INSERT INTO KATEGORI_UTAMA VALUES ('K10','Laptop & Aksesoris');
INSERT INTO KATEGORI_UTAMA VALUES ('K11','Komputer & Aksesoris');
INSERT INTO KATEGORI_UTAMA VALUES ('K12','Elektronik');
INSERT INTO KATEGORI_UTAMA VALUES ('K13','Kamera, Foto & Video');
INSERT INTO KATEGORI_UTAMA VALUES ('K14','Otomotif');
INSERT INTO KATEGORI_UTAMA VALUES ('K15','Olahraga');
INSERT INTO KATEGORI_UTAMA VALUES ('K16','Film, Musik & Game');
INSERT INTO KATEGORI_UTAMA VALUES ('K17','Dapur');
INSERT INTO KATEGORI_UTAMA VALUES ('K18','Office & Stationery');
INSERT INTO KATEGORI_UTAMA VALUES ('K19','Souvenir, Kado & Hadiah');
INSERT INTO KATEGORI_UTAMA VALUES ('K20','Mainan & Hobi');
INSERT INTO KATEGORI_UTAMA VALUES ('K21','Makanan & Minuman');
INSERT INTO KATEGORI_UTAMA VALUES ('K22','Buku');
