create or replace function update_stok()
returns "trigger" as $$
	declare old_stok1 integer;
	declare old_stok2 integer;
	declare new_stok1 integer;
	declare new_stok2 integer;	
	begin
		if(TG_OP = 'INSERT') then
			select stok into old_stok1 from shipped_produk as sp where sp.kode_produk = new.kode_produk; 
			new_stok1 = old_stok1 - new.kuantitas;
			update shipped_produk set stok = new_stok1 where kode_produk = new.kode_produk;
			return new;
		elsif(TG_OP = 'DELETE') then
			select stok into old_stok1 from shipped_produk as sp where sp.kode_produk = old.kode_produk; 
			new_stok1 = old_stok + old.kuantitas;
			update shipped_produk set stok = new_stok1 where kode_produk = old.kode_produk;
			return old;
		else
			select stok into old_stok1 from shipped_produk as sp where sp.kode_produk = old.kode_produk;
			new_stok1 = old_stok1 + old.kuantitas;
			update shipped_produk set stok = new_stok1 where kode_produk = old.kode_produk;
			select stok into old_stok2 from shipped_produk as sp where sp.kode_produk = new.kode_produk;
			new_stok2 = old_stok2 - new.kuantitas;
			update shipped_produk set stok = new_stok2 where kode_produk = new.kode_produk;
			return new;
		end if;
	end;
$$ language plpgsql;

create trigger update_stok
after insert or update or delete
on list_item for each row
execute procedure update_stok();

create or replace function add_poin()
returns "trigger" as $$
	declare old_poin integer;
	declare new_poin integer;
	declare total_harga numeric(10,2);
	begin 
		if(old.status <> 4 and new.status = 4) then
			
			select poin into old_poin from pelanggan where email = new.email_pembeli;
			new_poin = old_poin + new.total_bayar / 100;
			update pelanggan set poin = new.poin where email = new.email_pembeli;
			
		end if; 
		return new;
	end;
$$ language plpgsql;

create trigger add_poin
after update
on transaksi_shipped for each row
execute procedure add_poin();